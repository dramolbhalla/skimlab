var neo4j = require('neo4j');
var graphdb = new neo4j.GraphDatabase('http://skimlab.tgen.org:7474');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.once('open', function callback () {
	console.log('Successfully connected to MongoDB.');
});

exports.mongoose = mongoose;
exports.graphdb = graphdb;