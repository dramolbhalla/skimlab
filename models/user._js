var db = require('./../db')

var userSchema = new db.mongoose.Schema({
  username:  String,
  email: String,
  password: String,
  
  name: String,
  organization: String,
  occupation: String,
  
  datejoined:{ type: Date, default: Date.now },
  laslogin:{ type: Date, default: Date.now },
  
  haspermission:{ type: Boolean, default: true }
});

var User = db.mongoose.model('User', userSchema);

exports.User = User;