var db = require('./../db')
var user = require('./../models/user')

var crypto = require('crypto');

exports.get = function (req, res){

	res.render("login.html");
}

exports.post = function (req, res){

	var errors = [];
	
	existing = user.User.findOne( {username : req.body.username}, function(err, existing)
	{
		//If taken, put an error
		if(existing)
		{
			var shasum = crypto.createHash('sha1');
			shasum.update(req.body.password);
			var hashedpass = shasum.digest('hex');
			
			if(hashedpass != existing.password)
			{
				errors.push("You password is incorrect.");
			}
		}
		else
		{
			errors.push("That username does not exist.");
		}
		
		if( errors.length > 0 )
		{
			res.render("login.html", { body : req.body, errors : errors});
		}
		else
		{
			req.session.user = existing.username;
			
			res.redirect("/");			
		}
	}
	
	);
}

exports.logout = function (req, res){
	req.session.user = null;
	res.redirect("/");
}