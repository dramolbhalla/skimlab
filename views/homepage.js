var db = require('./../db')
var user = require('./../models/user')

var crypto = require('crypto');

exports.get = function (req, res){

	user.User.findOne( {username : req.session.user}, function(err, user)
	{
		var emailhash = null;
		
		if(user)
		{
			var md5hash = crypto.createHash('md5');
			md5hash.update(user.email);
			emailhash = md5hash.digest('hex');
		}
		
		res.render("index.html", { user: user, emailhash : emailhash});
	}
	
	)
}