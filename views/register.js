var db = require('./../db')
var user = require('./../models/user')

var crypto = require('crypto');

var request = require('request');


var public_key = "6Lca8-ISAAAAAP1tcRrBMEyMBsK3xIy3xKLrpryl";
var private_key = "6Lca8-ISAAAAAFgVVKbSD9x545AU1w-1XiPUbNqO";

exports.get = function (req, res){

	res.render("register.html", { public_key : public_key });
}

exports.post = function (req, res){

	var errors = [];
	if( !req.body.username )
	{
		errors.push( "You must include a user name." );
	}
	else
	{
		if( req.body.username.length < 3 || req.body.username.length > 15)
		{
			errors.push( "Your username must be 3-15 characters." );
		}
		
		if( req.body.username.match(/[^a-zA-Z0-9]/) )
		{
			errors.push("Your username can only contain letters and numbers.");
		}

	}
	
	//Make sure a user does not already exist with that username
	existing = user.User.findOne( {username : req.body.username}, function(err, existing)
	{
	
	//If taken, put an error
	if(existing)
	{
		errors.push("This username has already been taken.");
	}
	
	if( !req.body.pass1 ) //Verify that a password has been entered
	{
		errors.push("You must enter a password.");
	}
	else if( !req.body.pass2 ) //Verify that the passoword has been confirmed
	{
		errors.push("You must confirm your password.");
	}
	else if( req.body.pass1 != req.body.pass2) //Do the passwords match
	{
		errors.push("Your passwords do not match");
	}
	
	if( !req.body.email ) //Was an email entered?
	{
		errors.push("You must enter an email.");
	}
	else if( !req.body.email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/) ) //Is the email valid
	{
		errors.push("You must enter a valid email.");
	}
	
	var captchaform = {
	privatekey: private_key,
	remoteip : req.connection.remoteAddress,
	challenge : req.body.recaptcha_challenge_field,
	response : req.body.recaptcha_response_field
	}
	
	request.post('http://www.google.com/recaptcha/api/verify', {form : captchaform}, function (error, response, body)
	{
		if(body.indexOf("true") == -1)
		{
			errors.push("The Captcha was typed incorrectly.");
		}
		
		
		if( errors.length > 0 )
		{
			res.render("register.html", { body : req.body, errors : errors, public_key : public_key });
		}
		else
		{
			var newuser = new user.User( { } );
			newuser.username = req.body.username;
			newuser.email = req.body.email;
			
			var shasum = crypto.createHash('sha1');
			shasum.update(req.body.pass1);
			var hashedpass = shasum.digest('hex');
			
			newuser.password = hashedpass;
			
			newuser.name = req.body.name;
			newuser.organization = req.body.organization;
			newuser.occupation = req.body.occupation;
			
			newuser.save(
				function (err, newuser)
				{
					req.session.user = newuser.username;
					
					res.redirect("/");
				}
			);
		}
		
	}
	)
	

	
	}
	);
	

}