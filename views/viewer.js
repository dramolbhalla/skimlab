var db = require('./../db')
var user = require('./../models/user')

var async = require('async')

var relationships = [
  'START nodes=node(*)',
  'RETURN nodes'
].join('\n');

exports.get = function (req, res){

	res.render("graphviewer.html");

}

exports.loader = function(socket)
{
	console.log("Socket connection made from graph-viewer.");
	socket.emit('ready', { hello: 'world' });

	socket.on('getlinks', function (data)
	{
		console.log("getlinks");
		
		
		
		db.graphdb.query(relationships, {}, function(err, results)
		{
			if(!err)
			{
				async.each(results, function(item, callback)
				{
					socket.emit('node', { name: item.nodes.data.name });
					
					item.nodes.outgoing("influences", function(err, rels)
					{
						if(!err)
						{
						
							async.each(rels, function(rel, callback)
							{
								
								db.graphdb.getNodeById(rel.end.id, function(err, endnode)
								{
									//console.log(endnode.data.name);
									
									socket.emit('link', { start: item.nodes.data.name, end: endnode.data.name });
									
								});
								
								callback();
							});
						}
					});
					
					callback();
				});
			}
		});
	});
}