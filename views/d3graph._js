//Query a set of nodes and relationships for one disease to find relevant nodes and retrieve annotations from MongoDB. Can deal with indirect or direct relationships between nodes (node hops). No duplicates are returned.

console.time("tot");
//Custom variables:
	var collectionName = 'genes';
	var minhops = 1; var maxhops = 2; var params = {parameters: ["KIF1A", "LMBRD2"]};

//Initialization variables:
	var db = require('./../db');
	var user = require('./../models/user');
	var fs = require("fs");
	var csvdata = 'source,target,value'+'\n';
	var stodata = new Array();

exports.get = function (req, res){res.render("d3graph.html");}

var collection = db.mongoose.model(collectionName, db.mongoose.Schema());
var query = ['START startnode=node(*)', 'MATCH (startnode)-[r:influences*',minhops,'..',maxhops,']->(endnode)', 'WHERE startnode.name IN {parameters}', 'RETURN startnode,r,endnode'].join('\n');
var cb = function (err, results, _) {
	if (err || !results.length) console.log("No nodes found. The following is the error: " + err);
  	else {results.map_(_,queryGraph);}
  	console.timeEnd("tot");
};
function queryGraph(_,result,index){
	stodata[index] = result['endnode'].data.name;
	for(j=0;j<result['r'].length;j++){ 
		var thisnode = result['r'][j]; 
		csvdata += graphdb.getNodeById(thisnode.start.id,_).data.name + ',' + graphdb.getNodeById(thisnode.end.id,_).data.name + ',10' + '\n';
	} 
	if(index == (this.length-1)){
		fs.writeFile("public/graphdata.csv",csvdata,_);
		var mongodata = collection.find({'name':{$in:stodata}}, {'name':1,'microarrays':{$elemMatch:{'barcode':'TCGA-02-0006-01B-01R-0179-07'}},'_id':0},_).sort({'name':1});
		mongodata = '"<table><tr><th>Gene Name</th><th>Annotations</th></tr>' + mongodata + '</table>"'; 
		mongodata = mongodata.replace(/\n/g,'');
		mongodata = 'var mongodata = ' + mongodata + ';\n function setData(){mongodata = mongodata.replace(/\{ name:/g,"<tr><td>").replace(/,  microarrays/g,"</td><td>microarrays").replace(/\[ /g,"").replace(/\] \},/g,"</td></tr>").replace(/\] \}/g,"</td></tr>"); document.getElementById("mongodata").innerHTML = mongodata;}';
		fs.writeFile("public/mongodata.js",mongodata,_);
	}
}
console.log("The following is the stored annotation data for all genes between " + minhops + " to " + maxhops + " interactions from Genes " + params.parameters + ":"); 
db.graphdb.query(query,params,cb);
