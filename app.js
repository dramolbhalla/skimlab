require('streamline').register({
    fibers: false,
    cache: true,
    verbose: true,
});

//Load external modules
var express = require('express');
var cons = require('consolidate');
var swig = require('swig');


//Create web application
var app = express();
var https = require('https');
var http = require('http');

//Initialize DB Connections
var db = require('./db')

//Register SWIG as template rendered
app.engine('.html', cons.swig);
app.set('view engine', 'html')
swig.init({ root: 'templates/' });
app.set('views', 'templates/');

app.use(express.logger('dev')); //Log application

var sessionToken = require('crypto').randomBytes(48).toString('hex');
app.use(express.cookieParser(sessionToken)); //Set cookie password

app.use(express.session()); //Use sessions

app.use(express.bodyParser()) //handle POST requests

//Load Views
var homepage = require('./views/homepage')
var login = require('./views/login')
var register = require('./views/register')
var viewer = require('./views/viewer')
var d3graph = require('./views/d3graph')

//Register middle-ware
app.use(express.static(__dirname + '/public')); //Static files

app.get('/', homepage.get);

app.get('/login', login.get);
app.post('/login', login.post);

app.get('/logout', login.logout);

app.get('/register', register.get);
app.post('/register', register.post);

app.get('/viewer', viewer.get);

app.get('/d3graph', d3graph.get);

var server = http.createServer(app);
var io = require('socket.io').listen(server);

server.listen(80);

console.log('Express app started on port 3000');

io.sockets.on('connection', viewer.loader);


